package methodTest;

class Person{
	String name;
	
	public Person(String name) {
		this.name = name;
	}
}

public class App {
	public static void main(String[] args) {
		printName(20, "Alex");
		
		Person p1 = new Person("BOB");
		printPersonName(p1);
	}
	
	public static void printName(int x, String name) {
		x = x + 10;		
		System.out.println(name + "  : " + x);		
	}
	
	public static void printPersonName(Person p) { 
		System.out.println(p.name);
		p.name = "George";
		System.out.println(p.name);
	}
	
	
	//methods are the way to make our code more structured
	
}
