package jac;

class Plant extends Object{
	public void grow() {
		System.out.println("plant is growing");
	}
	
	public String toString() {
		return "Java is fun";
		
	}
	
}

class Tree extends Plant{
	
	//Overloading => polymorphism at compile time
	public void giveFruit() {
		System.out.println("here you are");
	}
	
	public void giveFruit(String name) {
		System.out.println("here you are this is " + name);
	}
	
	public void grow() {
		System.out.println("Tree is growing");
	}
	
}

class Cactus extends Plant{
	public void grow() {
		System.out.println("Cactus is growing");
	}
	
}


public class App {
	public static void main(String[] args) {
		/* overloading
		Tree t = new Tree();
		t.giveFruit("apple");
		t.giveFruit();
		*/
		
		Plant p = new Plant();
		p.grow();
		
		Tree t = new Tree();
		t.grow();
		
		//t = new Plant(); => it needs casting
		p = new Tree();
		p.grow();
		
		Plant palm = new Tree();
		palm.grow();
		
		//palm.giveFruit(); compile error because It is not defined in the PARENT class	
		doSomething(palm);
		
		Plant cactus = new Cactus();
		doSomething(cactus);
		
		Plant plant = new Plant();
		doSomething(plant);
		
		
		
		System.out.println(new Plant());
	}
	
	static void doSomething(Plant p) {
		System.out.println("calling from the method");
		p.grow();
	}
}
