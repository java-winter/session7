package banking;

import javax.print.attribute.standard.PrinterURI;

abstract class Bank{
	public abstract int getInterest();
}

class BankA extends Bank{

	@Override
	public int getInterest() {
		return 12;
	}
	
}

class BankB extends Bank{

	@Override
	public int getInterest() {
		return 13;
	}
	
}

class BankC extends Bank{

	@Override
	public int getInterest() {
		return 14;
	}
	
}

public class App {
	public static void main(String[] args) {
 		//printInterest(new Bank()); => Bank is an abstract class so you cannot initialize
		
		Bank b1 = new BankA();
		Bank b2 = new BankB();
		Bank b3 = new BankC();
		
		printInterest(b1);
		printInterest(b2);
		printInterest(b3);
		
//		printInterest(new BankA());
//		printInterest(new BankB());
//		printInterest(new BankC());
	}
	
	public static void printInterest(Bank bank) {
		System.out.println(bank.getClass().getName() + " " + bank.getInterest());
	}
	
}
